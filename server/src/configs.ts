import 'dotenv/config';

const {
  APP_PORT = 3000,
  DATABASE_URI = 'mongodb://admin:asdf@node-news-mongodb:27017/prod?authSource=admin',
} = process.env;

export const APP = {
  PORT: APP_PORT,
};

export const DATABASE = {
  URI: DATABASE_URI,
  NEWS_API: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
};
