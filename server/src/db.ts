import mongoose from 'mongoose';

import { DATABASE } from './configs';

main()
  .then(() => console.log('MongoDB connected'))
  .catch((err) => console.log('MongoDB conexion error', err));

async function main() {
  await mongoose.connect(DATABASE.URI);
}
