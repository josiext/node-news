import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import axios from 'axios';

import { DATABASE } from '../configs';
import NewsModel from '../news/news.schema';

export const seedNews = async () => {
  const res = await axios.get(DATABASE.NEWS_API);

  const data = res.data.hits?.map((item) => ({
    _id: item.objectID,
    title: item.title,
    storyTitle: item.story_title,
    createdAt: item.created_at,
    author: item.author,
    url: item.url,
    storyUrl: item.story_url,
  }));

  if (data) await NewsModel.insertMany(data);
};

@Injectable()
export class UpdateNewsService {
  private readonly logger = new Logger(UpdateNewsService.name);

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    return seedNews();
  }
}
