import { Schema, model } from 'mongoose';

const newsSchema = new Schema({
  _id: { type: String, required: true },
  title: String,
  storyTitle: String,
  createdAt: Date,
  author: String,
  url: String,
  storyUrl: String,
});

const NewsModel = model('News', newsSchema);

export default NewsModel;
