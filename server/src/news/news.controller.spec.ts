import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;

  beforeEach(() => {
    newsService = new NewsService();
    newsController = new NewsController(newsService);
  });

  describe('findAll', () => {
    it('should return an array of cats', async () => {
      const result = [
        {
          _id: '32024093',
          title: null,
          storyTitle: 'NimSkull: A Hard Fork of Nim',
          createdAt: new Date('2022-07-08T07:56:44.000Z'),
          author: 'pull_my_finger',
          url: null,
          storyUrl: 'https://github.com/nim-works/nimskull',
        },
      ];
      jest.spyOn(newsService, 'find').mockImplementation(async () => result);

      expect(await newsController.find(20)).toBe(result);
    });
  });
});
