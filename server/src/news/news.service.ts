import { Injectable } from '@nestjs/common';
import NewsModel from './news.schema';
import { News } from './news.types';

@Injectable()
export class NewsService {
  async find(limit: number): Promise<News[]> {
    let data = await NewsModel.find().limit(limit).sort({ createdAt: 'desc' });
    data = data.map((item) => item.toObject());
    return data;
  }
}
