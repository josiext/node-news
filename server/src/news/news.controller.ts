import { Controller, Get, Query } from '@nestjs/common';
import { NewsService } from './news.service';
import { News } from './news.types';

@Controller()
export class NewsController {
  constructor(private readonly appService: NewsService) {}

  @Get('news')
  find(@Query('limit') limit): Promise<News[]> {
    return this.appService.find(Number(limit ?? 10));
  }
}
