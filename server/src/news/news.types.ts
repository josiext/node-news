export interface News {
  _id: string;
  title?: string;
  storyTitle?: string;
  createdAt?: Date;
  author?: string;
  url?: string;
  storyUrl?: string;
}
