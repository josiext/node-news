import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { seedNews } from './task/update-news';

@Injectable()
export class AppService implements OnApplicationBootstrap {
  getStatus(): string {
    return 'ok';
  }
  async onApplicationBootstrap() {
    await seedNews();
  }
}
