import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsController } from './news/news.controller';
import { NewsService } from './news/news.service';
import { ScheduleModule } from '@nestjs/schedule';
import { UpdateNewsService } from './task/update-news';

@Module({
  imports: [ScheduleModule.forRoot()],
  controllers: [AppController, NewsController],
  providers: [AppService, NewsService, UpdateNewsService],
})
export class AppModule {}
