# Hacker News

![web app](./images/web.png "Web App")

The latest news about Node!

**Technologies:**
- React
- Nest.js
- Typescript
- Mongo / Mongoose
- Docker
- Docker Compose

## Start development mode

> Default docker-compose config use port 4000 for the web app and 3001 for the server, please make sure they are free on your machine.
> 
> Hot Reloading is not working on windows. [More info](https://github.com/facebook/create-react-app/issues/11879
)

1. On the computer terminal, in the root of the project execute: `docker-compose up`
2. go to http://localhost:4000/

## Start production mode

> Default docker-compose config use port 4000 for the web app and 3001 for the server, please make sure they are free on your machine.

1. On the computer terminal, in the root of the project execute: `docker-compose -f docker-compose.prod.yml up -d`
2. go to http://localhost:4000/