import { SERVER } from "../configs";
import { Storie } from "../utils/types";

const endpoint = "/news";

const find = async ({ limit = 10 }: { limit?: number } = {}): Promise<
  Storie[]
> => {
  const res = await fetch(`${SERVER.URL}${endpoint}?limit=${limit}`);
  if (!res.ok) throw new Error("News service not working");
  const data = await res.json();

  const dataFormatted = data.map((item: any) => ({
    ...item,
    createdAt: item.createdAt ? new Date(item.createdAt) : null,
  }));

  return dataFormatted;
};

const api = {
  find,
};

export default api;
