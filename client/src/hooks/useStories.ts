import { useEffect, useState } from "react";
import api from "../api";
import { Storie as IStorie } from "../utils/types";

const NEWS_REMOVED_KEY = "news-removed";

const useStories = (initValue: IStorie[]) => {
  const [stories, setStories] = useState<IStorie[]>(initValue);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    api
      .find({ limit: 20 })
      .then((v) => handleSetStories(v))
      .catch((e) => {
        console.error(e);
        setError(e);
      })
      .finally(() => setIsLoading(false));
  }, []);

  const handleSetStories = (stories: IStorie[]) => {
    const txt = localStorage.getItem(NEWS_REMOVED_KEY);
    const newsRemoved: IStorie["_id"][] = txt ? JSON.parse(txt) : [];

    stories = stories.filter((item) => !newsRemoved.includes(item._id));

    setStories(stories);
  };

  const onRemove = (id: IStorie["_id"]) => {
    const newStories = stories.filter((item) => item._id !== id);

    const txt = localStorage.getItem(NEWS_REMOVED_KEY);
    const newsRemoved: IStorie["_id"][] = txt ? JSON.parse(txt) : [];
    newsRemoved.push(id);

    localStorage.setItem("news-removed", JSON.stringify(newsRemoved));

    setStories(newStories);
  };

  return {
    stories,
    isLoading,
    error,
    onRemove,
  };
};

export default useStories;
