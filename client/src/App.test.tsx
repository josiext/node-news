import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders the app title", () => {
  render(<App />);
  const TitleEl = screen.getByText(/HN Feed/i);
  const SubtitleEl = screen.getByText(/we <3 hacker news/i);
  expect(TitleEl).toBeInTheDocument();
  expect(SubtitleEl).toBeInTheDocument();
});
