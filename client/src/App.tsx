import Header from "./components/Header";
import StoriesList from "./components/StoriesList";
import styles from "./App.module.css";

function App() {
  return (
    <div>
      <Header />
      <main>
        <section className={styles.stories_list}>
          <StoriesList />
        </section>
      </main>
    </div>
  );
}

export default App;
