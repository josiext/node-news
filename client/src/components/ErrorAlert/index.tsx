import styles from "./ErrorAlert.module.css";

export default function ErrorAlert({ show = false }: { show: boolean }) {
  return (
    <div
      style={{ display: show ? "block" : "none" }}
      className={styles.container}
    >
      <p>There was an error loading the news {":("}</p>
    </div>
  );
}
