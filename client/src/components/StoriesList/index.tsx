import useStories from "../../hooks/useStories";
import ErrorAlert from "../ErrorAlert";
import Storie from "../Storie";

export default function StoriesList() {
  const { stories, isLoading, error, onRemove } = useStories([]);

  const showStories = !isLoading && !error;

  return (
    <div>
      {isLoading && <p>Loading...</p>}
      <ErrorAlert show={Boolean(!isLoading && error)} />

      {showStories &&
        stories.map((item) => (
          <>
            {(item.title ?? item.storyTitle) && (
              <Storie
                key={item._id}
                data={item}
                onRemove={(d) => onRemove(d._id)}
                style={{ borderTop: 0, borderLeft: 0, borderRight: 0 }}
              />
            )}
          </>
        ))}
    </div>
  );
}
