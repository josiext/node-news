import styles from "./Storie.module.css";

import { Storie as IStorie } from "../../utils/types";
import TrashIcon from "../TrashIcon";
import { CSSProperties } from "react";
import getDateFormat from "../../utils/getDateFormat";

export interface StorieProps {
  data: IStorie;
  onRemove: (storie: IStorie) => void;
  style?: CSSProperties;
}

export default function Storie({ onRemove, data, style }: StorieProps) {
  const { storyUrl, url, storyTitle, title, author, createdAt } = data;

  return (
    <div className={styles.storie} style={style}>
      <a
        href={storyUrl || url || "#"}
        rel="noopener noreferrer"
        target="_blank"
        className={styles.content}
      >
        <p>
          {storyTitle ?? title}{" "}
          <span className={styles.storie_author}>{`- ${author} -`}</span>
        </p>

        <p>{createdAt ? getDateFormat(createdAt) : ""}</p>
      </a>
      <button onClick={(e) => onRemove(data)} className={styles.delete_btn}>
        <TrashIcon />
      </button>
    </div>
  );
}
