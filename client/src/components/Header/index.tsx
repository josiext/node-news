import styles from "./Header.module.css";

export default function Header() {
  return (
    <header className={styles.header}>
      <h1 className={styles.title}>HN Feed</h1>
      <h2 className={styles.subtitle}>We {"<3"} hacker news!</h2>
    </header>
  );
}
