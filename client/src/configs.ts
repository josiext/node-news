const { SERVER_URL = "http://localhost:3001" } = process.env;

export const SERVER = {
  URL: SERVER_URL,
};
