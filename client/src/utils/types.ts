export interface Storie {
  _id: string;
  storyUrl: string | null;
  url: string | null;
  createdAt: Date | null;
  author: string | null;
  storyTitle: string | null;
  title: string | null;
}
