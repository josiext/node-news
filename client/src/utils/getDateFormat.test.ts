import { getHours, getMinutes, subDays } from "date-fns";
import getDateFormat from "./getDateFormat";

describe("getDateFormat()", () => {
  test("returns a string", () => {
    const date = getDateFormat(new Date());

    expect(typeof date).toBe("string");
  });

  test("returns day and month", () => {
    const txtDate = "2022-04-23T18:25:00.000Z";

    const date = getDateFormat(new Date(txtDate));

    expect(date).toBe("Apr 23");
  });

  test("returns yersterday text", () => {
    const yesterday = subDays(new Date(), 1);

    const date = getDateFormat(yesterday);

    expect(date).toBe("Yesterday");
  });

  test("returns hour", () => {
    const today = new Date();
    const hour = getHours(today);
    const minutes = getMinutes(today);
    const time = hour >= 12 ? "pm" : "am";

    const date = getDateFormat(today);

    expect(date).toBe(`${hour}:${minutes} ${time}`);
  });
});
