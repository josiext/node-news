import { format } from "date-fns";

const getDateFormat = (date: Date) => {
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();

  const today = new Date();
  const todayYear = today.getFullYear();
  const todayMonth = today.getMonth();
  const todayDay = today.getDate();

  if (year === todayYear && month === todayMonth && day === todayDay)
    return format(date, "H:mm aaa");

  if (year === todayYear && month === todayMonth && day === todayDay - 1)
    return "Yesterday";

  return format(date, "LLL d");
};

export default getDateFormat;
